


# What I Learned

 **Arduino**
 
 - creating a server with a ethernet shield
 - creating a webpage on the server 
 - setting servo rotation based on HTTP 
 - using Cloud Firestore to make HTTP changes in real time 
 - creating a load balancer for separating tasks 
 - designating tasks of the brew process 
 - controlling the servo arm
