var cupClose = 0;
var currentWindowValue = window.location.href;

function setCupDrop () {
    db.collection('homeBase').doc('78kBh2c2OeNN8aTazMmz').update({
        cupDropData : cupClose
    })
}

function setCupSize () {
    db.collection('homeBase').doc('78kBh2c2OeNN8aTazMmz').update({
        cupSize : "Empty"
    })
}
function closeCupUrl () {
    $(location).attr('href', 'http://192.168.1.178');
}

function renderData(doc) {
    //setting up area for taking in db changes
    let cupDropDataRender;
    cupDropDataRender = doc.data().cupDropData;

    let cupSizeRender;
    cupSizeRender = doc.data().cupSize;
 
    var dataToken = [];
    dataToken.push(cupSizeRender, cupDropDataRender)

    if (dataToken[0] != "Empty" && dataToken[1] != 0) {
        db.collection('homeBase').doc('78kBh2c2OeNN8aTazMmz').update({
            didReceiveBrew : true
        })
    }
    else {
        didReceiveBrew : false
    }

    switch(cupSizeRender) {
        case 'Small':
            console.log("coffee size:", cupSizeRender)
            setTimeout(function(){  
                $(location).attr('href', 'http://192.168.1.178/?sizeSamll');
            }, 1000);
                setCupSize();
            break;

        case 'Medium':
            console.log("coffee size:", cupSizeRender)
            setTimeout(function(){  
                $(location).attr('href', 'http://192.168.1.178/?sizeMedium');
            }, 1000);
                setCupSize();
            break;

        case 'Large':
            console.log("coffee size:", cupSizeRender)
            setTimeout(function(){  
                $(location).attr('href', 'http://192.168.1.178/?sizeLarge');
            }, 1000);
                setCupSize();
            break;
    }
    if (cupDropDataRender === 1) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?cupDropOne');
        }, 1000);
        setCupDrop();
    }
    else if (cupDropDataRender === 2) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?cupDropTwo');
        }, 1000);
        setCupDrop(); 
        console.log("db true -- Cup 2");
    }
    else if (cupDropDataRender === 3) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?cupDropThree');
        }, 1000);
        setCupDrop();
    }
    else if (cupDropDataRender === 4) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?cupDropFour');
        }, 1000);
        setCupDrop();
    }
    else if (cupDropDataRender === 5) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?cupDropFive');
        }, 1000);
        setCupDrop();
    }
    //this is for hot water
    else if (cupDropDataRender === 6) {
        setTimeout(function(){  
            $(location).attr('href', 'http://192.168.1.178/?hotWater');   
        }, 1000);
        setCupDrop();
    }
    else if (cupDropDataRender === 0) {
        if (currentWindowValue === "http://192.168.1.178/") {
            console.log("good url no change")
        }
        else {
            setTimeout(function(){  
                closeCupUrl();
        }, 1000);
        }
    }
}

//real time listener 
db.collection('homeBase').orderBy('cupDropData').onSnapshot(snapshot => {
    let changes = snapshot.docChanges();
    changes.forEach(change => {
        renderData(change.doc);
    });
});