#include <SPI.h>
#include <Ethernet.h>
#include <Servo.h>

//adding servo names
Servo servoCupOne;
Servo servoCupTwo;
Servo servoCupThree;
Servo servoCupFour;
Servo servoCupFive;

int pos = 0; 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };   //physical mac address
byte ip[] = { 192, 168, 1, 178 };                      // ip in lan (that's what you need to use in your browser. ("192.168.1.178")
byte gateway[] = { 192, 168, 1, 1 };                   // internet access via router
byte subnet[] = { 255, 255, 255, 0 };                  //subnet mask
EthernetServer server(80);                             //server port     
String readString;

void setup() {
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

//attaching servos to pins
servoCupOne.attach(1);
servoCupTwo.attach(2);
servoCupThree.attach(3);
servoCupFour.attach(4);
servoCupFive.attach(5);



  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}


void loop() {
  // Create a client connection
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {   
      if (client.available()) {
        char c = client.read();
     
        //read char by char HTTP request
        if (readString.length() < 100) {
          //store characters to string
          readString += c;
          //Serial.print(c);
         }

         //if HTTP request has ended
         if (c == '\n') {          
           Serial.println(readString); //print to serial monitor for debuging
     
           client.println("HTTP/1.1 200 OK"); //send new page
           client.println("Content-Type: text/html");
           client.println();     
           client.println("<HTML>");
           client.println("<HEAD>");
           client.println("<TITLE>arduino side</TITLE>");
           client.println("</HEAD>");
           client.println("<BODY>");
           client.println("<h1> NOT FOR CLIENT VIEW</h1>"); 
           client.println("<hr />");
           client.println("<span id='htmlDataOut'></span>");  
           client.println("<span id='closeCupURL'></span>");
           client.println("<a id='dispenseCup' href=\"/?cupDropOne\"\">test servo Dispense </a>"); //for test only 
           client.println("</BODY>");
           client.println("<script src='https://www.gstatic.com/firebasejs/5.7.1/firebase.js'></script>");
           client.println("<script src='http://alexcevicelow.space/firebaseConf.js'></script>");
           client.println("<script src='http://alexcevicelow.space/localQuery.js'></script>");
           client.println("<script src='http://alexcevicelow.space/arduinoServerSide.js'></script>");
           client.println("</HTML>");
           
           
     
           delay(1);
           //stopping client
           client.stop();
  
//CUP ONE 
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?cupDropOne") >0){
                for(pos = 0; pos < 95; pos += 3)  // goes from 0 degrees to 180 degrees 
                {                                  // in steps of 1 degree 
                  servoCupOne.write(pos);              // tell servo to go to position in variable 'pos' 
                  delay(5);                       // waits 15ms for the servo to reach the position 
                }
                                for (pos = 180; pos >=0; pos -= 3){
                  servoCupOne.write(pos);
                  delay(10); 
                } 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################

//CUP Two 
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?cupDropTwo") >0){
                for(pos = 0; pos < 95; pos += 3)  // goes from 0 degrees to 180 degrees 
                {                                  // in steps of 1 degree 
                  servoCupTwo.write(pos);              // tell servo to go to position in variable 'pos' 
                  delay(5);                       // waits 15ms for the servo to reach the position 
                }
                                for (pos = 180; pos >=0; pos -= 3){
                  servoCupTwo.write(pos);
                  delay(10); 
                } 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################

//CUP Three 
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?cupDropThree") >0){
                for(pos = 0; pos < 95; pos += 3)  // goes from 0 degrees to 180 degrees 
                {                                  // in steps of 1 degree 
                  servoCupThree.write(pos);              // tell servo to go to position in variable 'pos' 
                  delay(5);                       // waits 15ms for the servo to reach the position 
                }
                                for (pos = 180; pos >=0; pos -= 3){
                  servoCupThree.write(pos);
                  delay(10); 
                } 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################

//CUP Four 
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?cupDropFour") >0){
                for(pos = 0; pos < 95; pos += 3)  // goes from 0 degrees to 180 degrees 
                {                                  // in steps of 1 degree 
                  servoCupFour.write(pos);              // tell servo to go to position in variable 'pos' 
                  delay(5);                       // waits 15ms for the servo to reach the position 
                }
                                for (pos = 180; pos >=0; pos -= 3){
                  servoCupFour.write(pos);
                  delay(10); 
                } 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################

//CUP Five 
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?cupDropFive") >0){
                for(pos = 0; pos < 95; pos += 3)  // goes from 0 degrees to 180 degrees 
                {                                  // in steps of 1 degree 
                  servoCupFive.write(pos);              // tell servo to go to position in variable 'pos' 
                  delay(5);                       // waits 15ms for the servo to reach the position 
                }
                                for (pos = 180; pos >=0; pos -= 3){
                  servoCupFive.write(pos);
                  delay(10); 
                } 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################

//Hot water
//##########################
           //main code for dropping cup 
           if (readString.indexOf("?hotWater") >0){
            //TODO add lid closing here 
           }
           //clearing string for next read
            readString="";  
            // end main cup drop code 
            
//##########################





     

           
         }
       }
    }
}
}
