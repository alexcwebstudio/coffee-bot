# Brew Buddy

Brew Buddy is a software and hardware solution for controlling the Keurig coffee maker remotely through an iOS application.This project is built on 3 main technologies: iOS, Arduino & Firebase.



# What I Learned

**iOS**

 - login system with firebase
 - real time database with cloud firestore
 - loginless authentication based on device ID
 - auto layout through storyboards 
 - UI collection views 
 - creating a custom launch screen
 - installing and using cocoaPods
 
 **Arduino**
 
 - creating a server with a ethernet shield
 - creating a webpage on the server 
 - setting servo rotation based on HTTP 
 - using Cloud Firestore to make HTTP changes in real time 
 - creating a load balancer for separating tasks 
 - designating tasks of the brew process 
 - controlling the servo arm

**Firebase**

 - configuring cloud firestore for iOS
 - installing firebase cocoaPods
 
## Data Model  
```mermaid
graph LR
A[iOS App ] -- Coffee Data --> B((Cloud Firestore ))
B --Coffee Data --> D{Arduino}
D --Status --> E((Cloud Firestore))  
E --Status Of Brew  --> A
