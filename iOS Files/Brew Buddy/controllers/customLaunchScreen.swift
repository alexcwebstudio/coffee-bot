//
//  customLaunchScreen.swift
//  Brew Buddy
//
//  Created by Alex Cevicelow on 2/2/19.
//  Copyright © 2019 Alex Cevicelow. All rights reserved.
//

import UIKit
import Firebase

class customLaunchScreen: UIViewController {
    var db: Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true;
        
        activityMonitor()
        firebaseConfig()
        lookForDeviceID()
        
    }
    
    private func lookForDeviceID () {
        if (UserDefaults.standard.string(forKey: "settingAuth") == nil) {
            OperationQueue.main.addOperation {
                [weak self] in
                self?.performSegue(withIdentifier: "goLogin", sender: self)
            }
        }
        else if (UserDefaults.standard.string(forKey: "settingAuth") != nil){
            checkDeviceID()
        }
        else {
            presentAlert(withTitle: "Error", message: "Unknow error setting auth")
        }
    }
    
    private func firebaseConfig () {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }
    
    private func checkDeviceID() {
        
        let user = Auth.auth().currentUser
        if let user = user {
            let uid = user.uid
            
            let docRef = db.collection("homeBase").document(uid)
            
            docRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    //looking for the device id within firestore using query
                    if let serverDeviceID = document.data()?["deviceID"] as? String{
                        //testing to see if local device id is the same as server
                        let localDeviceID = UIDevice.current.identifierForVendor?.uuidString
                        if localDeviceID == serverDeviceID {
                            self.performSegue(withIdentifier: "customLaunchLoginGood", sender: nil)
                        }
                        else{
                            self.performSegue(withIdentifier: "goLogin", sender: nil)
                        }
                    }
                    else {
                        self.presentAlert(withTitle: "error", message: "error getting devie id")
                    }
                }
                else {
                    self.presentAlert(withTitle: "error", message: "Cannot find document")
                }
            }
        }
    }
}
