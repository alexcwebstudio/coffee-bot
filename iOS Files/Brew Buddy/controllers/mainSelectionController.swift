//
//  mainSelectionController.swift
//
//
//  Created by Alex Cevicelow on 4/1/19.
//

import UIKit
import Firebase

class mainSelectionController: UIViewController {
    var db: Firestore!
    
    @IBOutlet weak var mainStartBrew: UIButton!
    @IBOutlet weak var selectSize: UIButton!
    
    var imageArray = [UIImage(named: "cap"),
                      UIImage(named: "vanilla"),
                      UIImage(named: "moringblend"),
                      UIImage(named: "donut"),
                      UIImage(named: "hotWater"),
                      UIImage(named: "hazel"),]
    
    private var currentSelectedFlavorLocal = 10
    var serverCupDrop = Int()
    private var serverCupSize: ServerCupSize = .small
    
    enum ServerCupSize: String {
        case small, medium, large
        func getSizing() -> String{
            switch self {
            case .small:
                return "Small"
            case .medium:
                return "Medium"
            case .large:
                return "Large"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseConfig()
        brewButtonCustom()
        customSelectButton()
        
    }
    
    private func brewButtonCustom() {
        mainStartBrew.layer.cornerRadius = 10
    }
    
    private func firebaseConfig () {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }
    
    private func customSelectButton () {
        selectSize.layer.borderWidth = 1.0
        selectSize.layer.borderColor = CGColor.cgSecondaryGreen
        selectSize.layer.backgroundColor = UIColor.white.cgColor
        selectSize.layer.cornerRadius = 5.0
    }
    
    private func coffeeFlavorServer() {
        switch currentSelectedFlavorLocal {
        case 0:
            serverCupDrop = 1
        case 1:
            serverCupDrop = 2
        case 2:
            serverCupDrop = 3
        case 3:
            serverCupDrop =  4
        case 4:
            serverCupDrop = 5
        case 5:
            serverCupDrop = 6
        default:
            presentAlert(withTitle: "Hold It!", message: "Please Select A Coffee Flavor")
        }
    }
    
    @IBAction func coffeeSizeButton(_ sender: UIButton) {
        
        sizeButtonAnimations()
        
        sender.tag += 1
        if sender.tag > 2 { sender.tag = 0}
        
        //changing title on size button
        switch sender.tag {
        case 1:
            serverCupSize = .medium
            selectSize.setTitle("\(serverCupSize)", for: .normal)
        case 2:
            serverCupSize = .large
            selectSize.setTitle("\(serverCupSize)", for: .normal)
        default:
            serverCupSize = .small
            selectSize.setTitle("\(serverCupSize)", for: .normal)
        }
    }
    
    private func sizeButtonAnimations () {
        UIView.animate(withDuration: 0.2, animations: {
            self.selectSize.titleLabel?.textColor = .white
        }, completion: { _ in
            additionalButtn()
        })
        func additionalButtn() {
            UIView.animate(withDuration: 0.2, animations: {
                self.selectSize.layer.backgroundColor =  UIColor.white.cgColor
            })
        }
    }
    
    @IBAction func startBrewAction(_ sender: Any) {
        if (currentSelectedFlavorLocal == 10) {
            presentAlert(withTitle: "Hold It!", message: "Please Select A Coffee Flavor")
        }
        else {
            updateServoState()
            OperationQueue.main.addOperation {
                [weak self] in
                self?.performSegue(withIdentifier: "goingToPostBrew", sender: self)
            }
        }
    }
    
    private func updateServoState() {
        let servoUpdate = db.collection("homeBase").document("78kBh2c2OeNN8aTazMmz")
        
        servoUpdate.updateData([
            "cupDropData": serverCupDrop,
            "cupSize": "\(serverCupSize.getSizing())",
        ]) { err in
            if err != nil {
                self.presentAlert(withTitle: "error", message: "error updating document")
            } else {}
        }
    }
}

extension mainSelectionController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        
        cell.layer.cornerRadius = 20
        cell.imgImage.image = imageArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentSelectedFlavorLocal = indexPath[1]
        coffeeFlavorServer()
    }
}
