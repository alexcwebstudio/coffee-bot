//
//  clickToHideText.swift
//  Brew Buddy
//
//  Created by Alex Cevicelow on 3/21/19.
//  Copyright © 2019 Alex Cevicelow. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func activityMonitor () {
        let actMon:UIActivityIndicatorView = UIActivityIndicatorView()
        actMon.center = self.view.center
        actMon.hidesWhenStopped = true
        actMon.style = UIActivityIndicatorView.Style.gray
        view.addSubview(actMon)
        actMon.startAnimating()
    }
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UIColor {
    public class var uiprimaryGreen: UIColor {
        return UIColor(red: 0, green: 255, blue: 147, alpha: 1.00)
    }
    public class var uisecondaryGreen: UIColor {
        return UIColor(red: 0, green: 227, blue: 119, alpha: 1)
    }
}

extension CGColor {
    public class var cgSecondaryGreen: CGColor {
        return UIColor(red: 0.0/255.0, green: 227/255.0, blue: 119/255.0 , alpha: 1.0).cgColor
    }
}
