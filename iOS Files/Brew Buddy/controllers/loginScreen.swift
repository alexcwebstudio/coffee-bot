//
//  loginScreen.swift
//  Brew Buddy
//
//  Created by Alex Cevicelow on 1/31/19.
//  Copyright © 2019 Alex Cevicelow. All rights reserved.
//

import UIKit
import Firebase

class loginScreen: UIViewController {
    var db: Firestore!
    
    @IBOutlet weak var emailField: HoshiTextField!
    @IBOutlet weak var getStarted: UIButton!
    @IBOutlet weak var passwordField: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true;
        self.hideKeyboard()
        getStartedRound()
        firebaseConfig()
    }
    
    private func getStartedRound () {
        getStarted.layer.cornerRadius = 5
    }
    
    private func firebaseConfig () {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }
    
    @IBAction func loginTap(_ sender: Any) {
        if let email = self.emailField.text, let password = self.passwordField.text {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if error != nil {
                    self.presentAlert(withTitle: "Warning", message: "Inccorect Username or Password")
                }
                self.settingDeviceId()
                
                OperationQueue.main.addOperation {
                    [weak self] in
                    self?.performSegue(withIdentifier: "goHomeAfterLogin", sender: self)
                }
            }
        }
        else {
            self.presentAlert(withTitle: "ERROR", message: "Undefined Error")
        }
    }
    
    
    private func settingDeviceId() {
        let testingDataChange = db.collection("homeBase").document("oPqMqzTLx4g12wVPfHzgFhrAsxa2")
        let settingDeviceID = UIDevice.current.identifierForVendor?.uuidString
        UserDefaults.standard.set(settingDeviceID, forKey: "settingAuth")
        
        testingDataChange.updateData([
            "deviceID": settingDeviceID as Any
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
            }
        }
    }
}
